#!/bin/bash

set -e

DIR="$( cd "$( dirname "$0" )" && pwd )"

cd $DIR

package=$(< ./package.json)

lint() {
    cd $DIR
    eslint src --ext .js
}

doc() {
    cd $DIR
    rm -fr jsdoc/*
    jsdoc -r -c ./jsdoc.json -d ./jsdoc/
}

test() {
    cd $DIR
    node ./tests/run.js
}

build() {
    cd $DIR
    echo "build"
}

deps() {
    echo -e "Cloning dependencies id deps directory..."
    cd $DIR
    # clear
    rm -fr deps/*/
    # read deps
    local urls=$(echo "$package" | jq -r .deps[])
    # clone
    cd $DIR"/deps"
    for url in ${urls[@]}; do
        git clone $url
    done
    echo -e "Cloning done."
}

symlinks() {
    echo -e "Creating symlinks..."
    cd $DIR
    # read symlinks
    local linkdirs=($(echo "$package" | jq -r '.symlinks | keys[]'))
    for linkdir in ${linkdirs[@]}; do
        echo -e "  > Creating symlinks in "$linkdir
        cd $DIR
        local targets=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | keys_unsorted[]'))
        local links=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | .[]'))
        # create symlinks
        cd $DIR"/"$linkdir
        for (( i=0; i<${#targets[@]}; i++ )); do
            link=${links[$i]}
            target=${targets[$i]}
            if [[ -f $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            elif [[ -d $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            else
                ln -sf $target $link
                echo -e "    > "$target" -> "$linkdir$link" created."
            fi
        done
    done
}

case $1 in
    deps)
        deps;;
    symlinks)
        symlinks;;
    setup)
        deps
        symlinks
        ;;
    lint)
        lint;;
    test)
        test;;
    jsdoc)
        doc;;
    build)
        build;;
esac
