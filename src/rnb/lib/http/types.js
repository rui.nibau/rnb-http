// Obliger de déclarer des types en jsdoc pour la documentation

/**
 * @typedef {{[name:string]: string}} HttpVars
 */

/**
 * @callback RouteHandler
 * @param {HttpVars} [vars]
 * @param {URLSearchParams} [params]
 * @param {string} [path]
 * @returns {Promise<any>}
 */

/**
 * Extending IncomingMessage to add useful methods
 *
 * @typedef RnbRequest
 * @property {() => Promise<Uint8Array>} arrayBuffer
 * @property {() => Promise<string>} text
 * @property {() => Promise<object>} json
 * @typedef {import('node:http').IncomingMessage & RnbRequest} RnbIncomingMessage
 */

/**
 * Warning: server route handler needs to end the response (res) when it has done its job.
 *
 * @callback ServerRouteHandler
 * @param {Request | RnbIncomingMessage} req
 * @param {import('node:http').ServerResponse} [res]
 * @param {HttpVars} [vars]
 * @param {URLSearchParams} [params]
 * @returns {Promise<Response | import('node:http').ServerResponse>}
 */

/**
 * @callback ServerErrorHandler
 * @param {Request | RnbIncomingMessage} req
 * @param {import('node:http').ServerResponse} res
 * @param {Error} error: Error
 * @returns {Promise<Response | import('node:http').ServerResponse>}
 */

/**
 * @typedef Route
 * @property {string} path
 * @property {RouteHandler | ServerRouteHandler} handler
 * @property {HttpVars} [vars]
 * @property {URLSearchParams} [params]
 */

/**
 * @callback RouteErrorHandler
 * @param {Error} error
 * @returns {Promise<any>}
 */

/**
 * @typedef ClientRedirect
 * @property {string} from
 * @property {RegExp} [regex]
 * @property {string} to
 */

/**
 * @typedef ClientExclude
 * @property {string} [pattern]
 * @property {RegExp} [regex]
 */


/**
 * @typedef RnbServerResponseBody
 * @property {boolean} ok
 * @property {number} code
 * @property {string} message
 */
