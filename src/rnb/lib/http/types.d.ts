// // Client types
// // --------------------------------------------------------

// /**
//  * Defines a redirect rule
//  */
// interface ClientRedirect {
//     /**
//      * Original url
//      */
//     from: string
//     regex?: RegExp
//     /**
//      * Destinatioon url
//      */
//     to: string
// }
// /**
//  * Defines an exclude rule from routing
//  */
// interface ClientExclude {
//     /**
//      * Pattern that the url must march
//      */
//     pattern?: string
//     /**
//      * Regex that the url must match
//      */
//     regex?: RegExp
// }
// /**
//  * Detail of a {@link ClientDispatchEvent}
//  */
// interface ClientDispatchEventDetail {
//     /**
//      * The error during the dispatch
//      */
//     error?: Error
//     /**
//      * Path
//      */
//     path: string,
//     time:number
// }

// /**
//  * Event emitted when a Client dispatch.
//  */
// type ClientDispatchEvent = CustomEvent<ClientDispatchEventDetail>

type ServerOptions = {
    /**
     * The port to listen on. (8000)
     */
    port?: number
    /**
     * hostname
     */
    hostname?: string
    /**
     * Private key in PEM format. RSA, EC, and PKCS8-format keys are supported.
     */
    key?: string
    /**
     * Certificate chain in PEM format.
     */
    cert?: string

    signal?: AbortSignal
}

// Request progress
// --------------------------------------------------------

/**
 * Upload progress options.
 */
interface UploadProgressOptions {
    /**
     * Upload mode. Default is 'reader'
     */
    mode?: ('bandwidth'|'reader'|'xhr'|'stream') = 'reader',
    /**
     * 
     */
    bandwidthUrl?: string
}

/**
 * Map between an url and a blob object
 */
type RequestFilesMap = {
    [url?:string]: Blob
}

/**
 * Callback called a the end of a load
 * 
 * @param file File loaded
 * @param url File url
 * @returns If true, load process is stopped
 */
type RequestFileLoadEnd = (file: Blob, url: string) => boolean

/**
 * Event emmited during upload profress.
 * 
 * Detail is percentage of progress (0 - 100)
 */
type RequestProgressEvent = CustomEvent<number>
