/**@module rnb/lib/http/Router */

import { buildRouteRegex, getPathname } from './httpUtils.js';

/**
 * Manage routes for a given path.
 */
export class PathHandler {
    /**
     * @param {Router} router Router
     * @param {string} path Path
     */
    constructor(router, path) {
        /**@private */
        this.router = router;
        /**@private */
        this.path = path;
    }
    /**
     * @param {ServerRouteHandler} handler Head request handler
     * @returns {PathHandler} This
     */
    head(handler) {
        this.router.add('head', this.path, handler);
        return this;
    }
    /**
     * @param {ServerRouteHandler | RouteHandler} handler Get request handler
     * @returns {PathHandler} This
     */
    get(handler) {
        this.router.add('get', this.path, handler);
        return this;
    }
    /**
     * @param {ServerRouteHandler} handler Get request handler
     * @returns {PathHandler} This
     */
    patch(handler) {
        this.router.add('patch', this.path, handler);
        return this;
    }
    /**
     * @param {ServerRouteHandler} handler Request handler
     * @returns {PathHandler} This
     */
    post(handler) {
        this.router.add('post', this.path, handler);
        return this;
    }
    /**
     * @param {ServerRouteHandler} handler Request handler
     * @returns {PathHandler} This
     */
    put(handler) {
        this.router.add('put', this.path, handler);
        return this;
    }
    /**
     * @param {ServerRouteHandler} handler Request handler
     * @returns {PathHandler} This
     */
    delete(handler) {
        this.router.add('delete', this.path, handler);
        return this;
    }
}

/**
 * Manage routes
 */
export default class Router {

    /**
     * @type {Object<string, Map<RegExp, RouteHandler | ServerRouteHandler>>}
     * @private
     */
    routes = {};

    /**
     * @type {Object<string, RouteHandler | ServerRouteHandler>}
     * @private
     */
    globalRoutes = {};

    /**
     * Remove all routes
     */
    dispose() {
        Object.values(this.routes).forEach(map => map.clear());
        this.routes = {};
        this.globalRoutes = {};
    }

    /**
     * Find a route for the given path and method.
     * 
     * @param {string} method Http Method
     * @param {string} path Path
     * @returns {Route} Route The Route or null
     */
    find(method, path) {
        method = method.toLowerCase();
        path = getPathname(path);
        if (method in this.routes) {
            for (const [regex, handler] of this.routes[method]) {
                const matches = regex.exec(path);
                if (matches !== null) {
                    return { path, handler, vars: matches.groups, 
                        params: new URLSearchParams(globalThis.location ? globalThis.location.search : '')};
                }
            }
        }
        if (method in this.globalRoutes) {
            return {path, handler:this.globalRoutes[method], vars: {}};
        }
        return null;
    }

    /**
     * Add an handler to all requests for the given http method. The handler will be called if no specific handler
     * (defined with the add() API) is found.
     * 
     * @param {string} method HTTP method
     * @param {RouteHandler | ServerRouteHandler} handler Request handler
     */
    all(method, handler) {
        this.globalRoutes[method] = handler;
    }

    /**
     * @param {string} method HTTP method
     * @param {string} path Path
     * @param {RouteHandler | ServerRouteHandler} handler Request handler
     */
    add(method, path, handler) {
        if (!this.routes[method]) {
            this.routes[method] = new Map();
        }
        this.routes[method].set(buildRouteRegex(path), handler);
    }

    /**
     * @param {string} path Path to manage
     * @returns {PathHandler} Path handler for the given path
     */
    path(path) {
        return new PathHandler(this, path);
    }
}
