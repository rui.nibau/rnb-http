/**@module rnb/lib/http/Client */

import { PROTOCOL_FILE, buildRouteRegex, cleanPath } from './httpUtils.js';
import Router from './Router.js';

const RE_CLICK_EL = /(a|button|input|select|textarea)/;
const RE_DOWNLOAD = /\.[a-z0-9]{2,4}$/;

export const RE_TRAILING_SLASH = /\/$/;
export const RE_MULTIPLE_SLASH = /\/{2,}/g;
export const SLASH = '/';

/**
 * @param {ClientRedirect[]} redirects Redirects list
 * @returns {ClientRedirect[]} List updated
 */
const buildRedirects = redirects => {
    return redirects.map(redirect => {
        redirect.regex = buildRouteRegex(redirect.from);
        return redirect;
    });
};

class Urls {
    /**
     * @private
     * @type {ClientExclude[]}
     */
    excludesList = [];

    /**
     * @private 
     * @type {ClientRedirect[]}
     */
    redirectsList = null;
     
    /**
     * @private 
     * @type {string}
     */
    redirectsUrl = null; 

    /**
     * @param {ClientExclude[]} excludes Paths to Exclude from routing
     */
    excludes(excludes = []) {
        this.excludesList = excludes;
    }

    /**
     * @param {string} url Url
     * @returns {boolean} If it is excluded
     */
    isExclude(url) {
        if (!this.excludesList || this.excludesList.length === 0) {
            return false;
        }
        return this.excludesList.some(exclude => exclude.regex.test(url));
    }

    /**
     * A list of RouteRedirect objects or an url pointing to a json resource containing a list of
     * RouteRedirect objects.
     *
     * @param {ClientRedirect[] | string} redirects Redirects
     */
    redirects (redirects) {
        if (Array.isArray(redirects)) {
            this.redirectsList = buildRedirects(redirects);
        } else {
            this.redirectsUrl = redirects;
        }
    }

    /**
     * Try to redirect an old url to a new one
     *
     * @param {string} url Old url
     * @returns {Promise<string>} The new url or null
     */
    async redirect(url) {
        if (this.redirectsUrl && !this.redirectsList) {
            const response = await globalThis.fetch(this.redirectsUrl);
            if (response) {
                const json = await response.json();
                if (json && Array.isArray(json)) {
                    this.redirectsList = buildRedirects(json);
                }
            }
        }
        if (this.redirectsList) {
            const red = this.redirectsList.find(redirect => {
                if (redirect.regex) {
                    return redirect.regex.test(url);
                }
                return redirect.from === url;
            });
            if (red) {
                return red.to;
            }
        }
        return null;
    }

    /**
     * Find link when clicking the page
     *
     * @param {HTMLElement} el Dom event
     * @returns {string} If it's a app link
     */
    find(el) {
        let url = null,
            nodeName;
        while (el && el.parentElement) {
            nodeName = el.nodeName.toLowerCase();
            if (nodeName.match(RE_CLICK_EL)) {
                const href = el.getAttribute('href');
                if (!el.hasAttribute('download') && href && nodeName === 'a' && this.isValidHref(href)) {
                    url = href;
                }
                break;
            }
            el = el.parentElement;
        }

        return url && !this.isExclude(url) ? url : null;
    }

    /**
     * If it's a valid href (manageable by router).
     *
     * @param {string} href 
     * @returns {boolean}
     */
    isValidHref(href) {
        return (href.startsWith(SLASH) || href.startsWith('.')) && !href.match(RE_DOWNLOAD);
    }
}

/**
 * @typedef {CustomEvent<{error?:Error, path:string, time: number}>} ClientDispatchEvent
 */

/**
 * Client appalication to manage GET requests in the browser
 *
 * Read {@link client.md}
 */
export default class Client extends EventTarget {

    /**
     * A {@link ClientDispatchEvent} fired when a dispatch is successful. 
     * Detail contains dispatched path.
     */
    static EVENT_DISPATCH = 'client-dispatch';

    /**
     * A {@link ClientDispatchEvent} fired when an error occurend during client dispatch. 
     * Detail contains Error and dispatched path.
     */
    static EVENT_DISPATCH_ERROR = 'client-dispatch-error';

    /**
     * A {@link ClientDispatchEvent} fired when a requested path as no route. Detail contains the path requested
     */
    static EVENT_NO_ROUTE = 'client-no-route';

    /**
     * @type {Router}
     * @private
     */
    router = null;

    /**
     * @type {string} Current dispatched pathname
     */
    currentPathname;

    constructor() {
        super();
        this.router = new Router();
        this.urls = new Urls();
        this.url = new URL(globalThis.location.href);

        globalThis.document.body.addEventListener('click', this);
        if (globalThis instanceof EventTarget) {
            try {
                globalThis.addEventListener('popstate', this);
            } catch(err) {
                // protection on Deno
                console.log('Client.constructor', err.message);
            }
        }
    }

    dispose() {
        this.router.dispose();
        globalThis.document.removeEventListener('click', this);
        if (globalThis instanceof EventTarget) {
            globalThis.removeEventListener('popstate', this);
        }
    }
    /**
     * 
     * @param {Client.EVENT_DISPATCH | Client.EVENT_DISPATCH_ERROR | Client.EVENT_DISPATCH_ERROR} event 
     * @param {(e:ClientDispatchEvent) => void | boolean} listener 
     */
    addListener(event, listener) {
        this.addEventListener(event, listener);
    }

    /**
     * Handle 'click' and 'popstate' to dispatch urls
     * 
     * @param {Event} e DOM event
     */
    async handleEvent(e) {
        switch(e.type) {
            case 'click': {
                const path = this.urls.find(/**@type {HTMLElement}*/(e.target));
                if (path) {
                    e.preventDefault();
                    const params = new URLSearchParams(globalThis.location.search.substring(1));
                    params.set('r', path);
                    globalThis.history.pushState({path}, '', this.isLocal() ? `${this.url.pathname}?${params}` : path);
                    await this.dispatch(path);
                }
                break;
            }
            case 'popstate': {
                // const state = /**@type {PopStateEvent}*/(e).state;
                if (!this.isLocal() && cleanPath(globalThis.location.pathname) !== this.currentPathname) {
                    await this.dispatch();
                } else if (this.isLocal()) {
                    await this.dispatch(/**@type {PopStateEvent}*/(e).state?.path);
                }
                break;
            }
        }
    }

    /**
     * Handle all requests.
     * 
     * @param {RouteHandler} handler Hndler of all requests
     */
    all(handler) {
        this.router.all('get', handler);
    }

    /**
     * Add a request handler for the given path
     * 
     * @param {string |string[]} path Path to manage
     * @param {RouteHandler} handler Handler 
     */
    add(path, handler) {
        if (Array.isArray(path)) {
            path.forEach(p => this.router.add('get', p, handler));
        } else {
            this.router.add('get', path, handler);
        }
    }

    /**
     * Manage redirections
     * 
     * @param {ClientRedirect[] | string} redirects Redirects list
     */
    redirects(redirects) {
        this.urls.redirects(redirects);
    }

    /**
     * Exclude some paths from routing.
     * 
     * @param {ClientExclude[]} excludes Paths to Exclude from routing
     */
    excludes(excludes = []) {
        this.urls.excludes(excludes);
    }

    isLocal () {
        return this.url.protocol === PROTOCOL_FILE;
    }

    /**
     * Push an url into history and dispatch
     * 
     * @param {string} path 
     */
    async push(path) {
        globalThis.history.pushState({path}, '', path);
        await this.dispatch();
    }

    /**
     * Dispatch a path request to call the related route.
     *
     * @param {string} [path] Path to dispatch
     * @fires {@link EVENT_DISPATCH}, {@link EVENT_DISPATCH_ERROR}, {@link EVENT_NO_ROUTE}
     */
    async dispatch(path) {
        const startTime = Date.now();
        if (!path) {
            path = this.isLocal() ? SLASH : globalThis.location.pathname;
        }
        // redirects
        const redirect = await this.urls.redirect(path);
        if (redirect) {
            this.push(redirect);
            return;
        }
        // route
        const route = this.router.find('get', path);
        if (route) {
            try {
                this.currentPathname = route.path;
                await /**@type {RouteHandler}*/(route.handler)(route.vars, route.params, path);
                this.dispatchEvent(new CustomEvent(Client.EVENT_DISPATCH, {
                    detail: {path, time: Date.now() - startTime}}));
            } catch(error) {
                this.dispatchEvent(new CustomEvent(
                    error instanceof PageError ? Client.EVENT_NO_ROUTE : Client.EVENT_DISPATCH_ERROR, 
                    {detail: {error, path, time: Date.now() - startTime}}
                ));
            }
        } else {
            this.dispatchEvent(new CustomEvent(Client.EVENT_NO_ROUTE, 
                {detail: {path, time: Date.now() - startTime}}));
        }
    }
}

/**
 * Error that can be thrown when a page cannot be reached. It will generate a {@link ClientNoRouteEvent}
 */
export class PageError extends Error {

}
