import { PROTOCOL_FILE } from './httpUtils.js';
import { defineRequestAlternative } from './request.js';

defineRequestAlternative(async url => {
    return new Promise((resolve, reject) => {
        try {
            if (url.startsWith(PROTOCOL_FILE)) {
                url = url.substring(PROTOCOL_FILE.length + 2);
                import('node:fs').then(fs => {
                    try {
                        resolve(new Response(fs.readFileSync(url)));
                    } catch(e) {
                        console.warn(e);
                        reject(e);
                    }
                });
            } else {
                import('node:http').then(http => {
                    http.get(url, res => {
                        if (res.statusCode >= 200 && res.statusCode < 300) {
                            res.on('data', data => resolve(new Response(data)));
                        } else {
                            resolve(new Response(null, {status: res.statusCode}));
                        }
                    });
                });    
            }
        } catch (err) {
            console.warn(err);
            reject(err);
        }
    });
});

export {};
