/**
 * Module with some usefull methods to send response to client
 *
 * @module rnb/lib/http/responses 
 */

/**@typedef {import('node:http').ServerResponse} ServerResponse */

const IS_DENO = typeof Deno === 'object';

export const MIME_TYPES = {
    cbz: 'application/zip',
    css: 'text/css',
    html: 'text/html',
    js: 'application/javascript',
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    json: 'application/json',
    pdf: 'application/pdf',
    png: 'image/png',
    xml: 'application/xml',
};

/**
 * @param {string} respath Path of a resource
 * @returns {string} Mime type based on the extension
 */
export const getMimetype = respath => {
    const dot = respath.lastIndexOf('.');
    const ext = dot > -1 ? respath.substring(dot+1) : null;
    return ext && MIME_TYPES[ext] ? MIME_TYPES[ext] : MIME_TYPES.html;
};

/**
 * @param {ServerResponse} res On node
 * @param {string|Blob} body 
 * @param {{[name:string]: string}} [headers] 
 * @param {number} status 
 * @returns {Response | ServerResponse}
 */
export const sendResponse = (res, body, headers, status = 200) => {
    if (IS_DENO) {
        return new Response(body, {headers, status});
    }
    res.writeHead(status || 200, headers);
    res.write(body);
    res.end();
    return res;
};

/**
 * @param {string} respath 
 * @returns {{[name: string]: string}}
 * @private
 */
const getResHeaders = respath => {
    const stats = stat(respath);
    const headers = {
        'content-length': String(stats.size),
        'content-type': getMimetype(respath),
    };
    if (stats.atime) {
        headers['date'] = stats.atime.toUTCString();
    }
    if (stats.mtime) {
        headers['last-modified'] = stats.mtime.toUTCString();
    }
    return headers;
};

/**
 * @param {ServerResponse} res Nodejs response
 * @param {string} respath Needs to be a valid path
 * @param {boolean} [isHead] Head request
 * @param {number} [status]
 * @returns {Response | ServerResponse}
 */
export const sendResource = (res, respath, isHead, status = 200) => {
    let body = null;
    try {
        body = isHead ? '' : readFile(respath);
    } catch(err) {
        status = 400;
    }
    return sendResponse(res, body, getResHeaders(respath), status);
};

/**
 * 
 * @param {ServerResponse} res 
 * @param {string} respath 
 * @returns {Response | ServerResponse}
 */
export const streamResource = (res, respath) => {
    if (IS_DENO) {
        const file = Deno.openSync(respath, {read: true});
        // FIXME How to close the file
        return new Response(file.readable, { headers: getResHeaders(respath) });    
    }
    // node.js
    res.writeHead(200, getResHeaders(respath));
    const stream = createReadableStream(respath);
    stream.on('open', () => stream.pipe(res));
    stream.on('error', err => {
        throw err; 
    });
    stream.on('end', () => res.end());
    return res;
};

/**
 * Send a json response
 * 
 * @param {ServerResponse} res
 * @param {object} json
 * @param {number} [status = 200]
 * @returns {Response | ServerResponse}
 */
export const sendJson = (res, json, status = 200) => {
    return sendResponse(res, JSON.stringify(json), {'Content-Type': MIME_TYPES.json}, status);
};


let stat = filepath => {
    return {size: 0}; 
};

let readFile = filepath => null;

let createReadableStream = respath => null;

// for nodejs 12 (raspi)
if (IS_DENO) {
    stat = respath => Deno.statSync(respath);
    readFile = respath => Deno.readFileSync(respath);
} else {
    const fs = await import('node:fs');
    stat = respath => fs.statSync(respath);
    readFile = respath => fs.readFileSync(respath);
    createReadableStream = respath => fs.createReadStream(respath);
}    

