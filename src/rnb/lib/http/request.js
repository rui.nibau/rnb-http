/**@module rnb/lib/http/request */

if (!globalThis.Response) {
    /** @ignore */
    // @ts-ignore
    globalThis.Response = class FakeResponse {
        /**
         * @param {string} body 
         * @param {object} [options] 
         */
        constructor (body, options = {}) {
            this.body = body;
            this.status = 200;
            this.statusText = 'OK';
            for (const [key, value] of Object.entries(options)) {
                this[key] = value;
            }
        }
        get ok() {
            return this.status >= 200 && this.status < 300;
        }
        async json () {
            return new Promise((resolve, reject) => {
                if (this.body && this.ok) {
                    try {
                        resolve(JSON.parse(this.body));
                    } catch (e) {
                        reject(e);
                    }
                } else {
                    reject();
                }
            });
        }
        async text () {
            return Promise.resolve(this.body);
        }
    };
}

const IS_NODE = typeof process !== 'undefined' && typeof Deno !== 'object';

/**
 * @param {string} url Url to reuqest
 * @param {RequestInit} options Options
 * @returns {Promise} Promise resolved if request is ok
 * @ignore
 */
const xhrReq = async (url, options = {}) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(options.method || 'GET', url);
        xhr.responseType = 'text';
        if (options.headers) {
            // FIXME add headers
        }
        xhr.onload = () => xhr.status < 400 ? resolve(new Response(xhr.responseText)) : reject(); 
        xhr.onerror = reject;
        xhr.send();
    });
};

/**
 * 
 * @param {string} url 
 * @param {RequestInit} [options] Options Same format as the fetch options
 * @returns {Promise<Response>} Resolved if request is good, rejected otherwise.
 */
let alternative = (url, options) => {
    return Promise.reject(new Response(`No request method to handle ${url}`, {status: 500}));
};

/**
 * Override request Alternative. Only useful on plateform that does not have fetch or XmlHttRequest of for local urls.
 * 
 * @param {(url: string, options?: RequestInit) => Promise<Response>} reqAlternative 
 */
export const defineRequestAlternative = reqAlternative => {
    alternative = reqAlternative;
};

/**
 * Forcer l'usage  de xhr avec webkit ou node pour des requêtes file://
 *
 * @param {string} url Url to request
 * @param {RequestInit} [options] Options Same format as the fetch options
 * @returns {Promise<Response>} Resolved if request is good, rejected otherwise.
 * @private
 */
const doRequest = 'fetch' in globalThis && !((IS_NODE || globalThis.webkitRequestAnimationFrame) 
        && globalThis.location && globalThis.location.protocol === 'file:')
    ? globalThis.fetch 
    : ('XMLHttpRequest' in globalThis 
        ? xhrReq 
        : (url, options) => alternative(url, options));

/**
 * globalThis.fetch wrapper to deal with 4xx responses : if response is not ok, promise is rejected.
 * 
 * If fetch is not present, fallback to XmlHttRequest or a NodeJs request.
 * 
 * Method as the same signature as the fetch API.
 *
 * @param {string} url Url to request
 * @param {RequestInit} [options] Options Same format as the fetch options
 * @returns {Promise<Response>} Resolved if request is good, rejected otherwise.
 */
export default async function request (url, options = {}) {
    const response = await doRequest(url, options);
    if (response && response.ok) {
        return response;
    }
    throw new Error(response ? `${response.status}: ${response.statusText}` : `Error requesting ${url}`);
}
