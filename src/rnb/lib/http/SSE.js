/**
 * @module rnb/lib/http/SSE
 * @ignore
 */

class SSEClient {
    /**
     * @param {Request | import('http').IncomingMessage} request Response
     * @param {Response | import('http').ServerResponse} response Response
     * @param {(client:SSEClient) => void} onclose
     */
    constructor(request, response, onclose) {
        this.id = Date.now();
        this.response = response;
        response.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive',
            'Cache-Control': 'no-cache',
            'transfer-encoding': 'chunked',
        });
        request.on('close', () => {
            onclose(this);
            this.close();
        });
    }
    close() {
        this.response = null;
        this.id = null;
    }
}

/**
 * Server Side Event
 *
 * @ignore
 */
export default class SSE {

    id = Date.now();

    /**
     * @type {SSEClient[]}
     * @private
     */
    clients = [];

    /**
     * @param {Request | import('node:http').IncomingMessage} request Response
     * @param {import('http').ServerResponse} response Response
     */
    addClient(request, response) {
        this.clients.push(new SSEClient(request, response, client => this.close(client)));
    }

    /**
     * Send data to clients
     * 
     * @param {object} data Data to send
     * @param {string} event
     * @param {SSEClient[]} [clients] Specific clients to send data to
     */
    send(data, event, clients) {
        let message = `id: ${this.id}\n`;
        if (event) {
            message += `event: ${event}\n`;
        }
        message += `data: ${typeof data === 'string' ? data : JSON.stringify(data)}\n\n`;
        (clients || this.clients).forEach(client => {
            client.response.write(message);
        });
    }

    /**
     * @param {SSEClient} [client] 
     */
    close(client) {
        if (!client) {
            this.clients.forEach(client => client.close());
            this.clients.length = 0;
        } else {
            this.clients = this.clients.filter(c => c.id !== client.id);
            client.close();
        }
    }
}
