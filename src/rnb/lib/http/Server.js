/**
 * @module rnb/lib/http/Server
 */

import Router from './Router.js';
import SSE from './SSE.js';
import { getPathname } from './httpUtils.js';
import { sendResponse } from './responses.js';

const IS_NODE = typeof Deno !== 'object';

/**
 * Simple class to instantiate an http server and to register routes. 
 * Works on Node et Deno.
 */
export default class Server {

    /**
     * @type {Router}
     * @private
     */
    router = null;

    /**
     * @type {{[key:string]: SSE}}
     * @private
     */
    sses = {};

    /**
     * @type {ServerErrorHandler}
     * @private
     */
    errorHandler = null;

    /**
     * @type {AbortController}
     */
    #ac = null;

    /**
     * @type {import('node:http').Server}
     * @private
     */
    server;

    #port = 8000;

    #hostname = 'localhost';
    
    #protocol = 'http';

    /**
     * @param {number} port Port
     * @param {ServerOptions} [options]
     */
    constructor(port = 8000, options = {}) {
        this.#port = port;
        if (!IS_NODE) {
            this.#ac = new AbortController();
            options.signal = this.#ac.signal;
        }
        if (options.key && options.cert) {
            this.#protocol = 'https';
        }
        if (options.hostname) {
            this.#hostname = options.hostname;
        }
        this.router = new Router();
        this.server = createServer(port, options, this.dispatch.bind(this));
    }
    
    /**
     * 
     * @returns {string} <protocol>://<hostname>:<port>
     */
    getAdress() {
        return `${this.#protocol}://${this.#hostname}:${this.#port}`;
    }
    
    /**
     * Stop server
     */
    stop() {
        if (this.#ac) {
            // Deno
            this.#ac.abort();
        } else {
            // node
            this.server.close();
            this.server.closeAllConnections();
        }
    }

    /**
     * @param {Request | RnbIncomingMessage} req Request
     * @param {import('node:http').ServerResponse} [res] Response object on nodejs
     * @returns {Promise<Response | import('node:http').ServerResponse>}
     */
    async dispatch(req, res = null) {
        // Manage nodejs case
        let isEventStream = false;
        if (IS_NODE) {
            // @ts-ignore
            req.url = new URL(req.url, `http://${req.headers.host}`).href;
            // req.headers = new Headers(req.headers);
            isEventStream = req.headers.accept && req.headers.accept === 'text/event-stream';
        } else {
            isEventStream = /**@type {Request}*/(req).headers.get('accept') === 'text/event-stream';
        }
        console.log(`Server.dispatch: ${req.method} ${req.url}`);
        /**@type {Error} */
        let error = null, code = 400;
        if (isEventStream) {
            const path = getPathname(req.url);
            if (path in this.sses) {
                this.sses[path].addClient(req, res);
            } else {
                error = new Error('Not an server-sent endpoint');
            }
        } else {
            const route = this.router.find(req.method, req.url);
            if (route) {
                try {
                    return await /**@type {ServerRouteHandler}*/(route.handler)(req, res, route.vars, route.params);
                } catch (e) {
                    console.error(e);
                    error = e;
                    code = 500;
                }
            } else {
                error = new Error('No route');
            }
        }

        if (error !== null) {
            if (this.errorHandler) {
                return await this.errorHandler(req, res, error);
            }
            return sendResponse(res, error.message, {}, code);
        }
    }

    /**
     * 
     * @param {ServerErrorHandler} handler Error Handler
     */
    handleError(handler) {
        this.errorHandler = handler;
    }

    /**
     * 
     * @param {string} path 
     * @param {object} data 
     * @param {string} [event]
     */
    pushEvent(path, data, event) {
        if (this.sses[path] && this.sses[path] !== null) {
            this.sses[path].send(data, event);
        }
    }

    /**
     * @param {string[]} methods 
     * @param {ServerRouteHandler} handler 
     */
    all(methods, handler) {
        methods.forEach(method => this.router.all(method, handler));
    }

    /**
     * Global handler for GET requests
     * 
     * @param {ServerRouteHandler} handler Request handler
     */
    allGet(handler) {
        this.router.all('get', handler);
    }

    /**
     * Global handler for HEAD requests
     * 
     * @param {ServerRouteHandler} handler Request handler
     */
    allHead(handler) {
        this.router.all('head', handler); 
    }
    
    /**
     * Global handler for PATCH requests
     * 
     * @param {ServerRouteHandler} handler Request handler
     */
    allPatch(handler) {
        this.router.all('patch', handler);
    }

    /**
     * Global handler for POST requests
     * 
     * @param {ServerRouteHandler} handler Request handler
     */
    allPost(handler) {
        this.router.all('post', handler);
    }

    /**
     * Global handler for PUT requests
     * 
     * @param {ServerRouteHandler} handler Request handler
     */
    allPut(handler) {
        this.router.all('put', handler);
    }
    
    /**
     * Global handler for DELETE requests
     * 
     * @param {ServerRouteHandler} handler Request handler
     */
    allDelete(handler) {
        this.router.all('delete', handler);
    }

    /**
     * @param {string} path Path to manage
     * @returns {import('./Router.js').PathHandler} PathHandler
     */
    path(path) {
        return this.router.path(path);
    }

    /**
     * @param {string} path Path
     * @param {ServerRouteHandler} handler Request handler
     */
    get(path, handler) {
        this.router.add('get', path, handler);
    }

    /**
     * @param {string} path Path
     * @param {ServerRouteHandler} handler HEAD Request handler
     */
    head(path, handler) {
        this.router.add('head', path, handler);
    }

    /**
     * @param {string} path Path
     * @param {ServerRouteHandler} handler PATCH Request handler
     */
    patch(path, handler) {
        this.router.add('patch', path, handler);
    }

    /**
     * @param {string} path Path
     * @param {ServerRouteHandler} handler POST Request handler
     */
    post(path, handler) {
        this.router.add('post', path, handler);
    }

    /**
     * @param {string} path Path
     * @param {ServerRouteHandler} handler DELETE Request handler
     */
    delete(path, handler) {
        this.router.add('delete', path, handler);
    }

    /**
     * Add a server-sent event manager to an endpoint
     * 
     * @param {string} path Path
     */
    sse(path) {
        this.sses[path] = new SSE();
    }
}

// compress:
// zlib.gunzip
// zlib.inflate

/**
 * 
 * @param {number} port 
 * @param {ServerOptions} options 
 * @param {Function} listener 
 * @returns {import('node:http').Server}
 */
let createServer = (port, options, listener) => {
    return null;
};

// for nodejs 12 (raspi)
if (!IS_NODE) {
    createServer = (port = 8000, options, listener) => {
        options.port = port;
        return Deno.serve(options, listener);
    };
} else {
    // nodejs
    const https = await  import('node:https');
    const { default: http, IncomingMessage } = await import('node:http');
    /**
     * @param {number} port Port
     * @param {ServerOptions} options ServerOptions
     * @param {Function} listener
     */
    createServer = (port = 8000, options, listener) => {
        const server = (options.key && options.cert ? https : http).createServer(Object.assign({
            IncomingMessage: RnbIncomingMessage,
            keepAlive: false,
        }, options), listener);
        server.addListener('close', () => console.log('Server closed'));
        server.listen(port, () => console.log(`Server listening port ${port}`));
        return server;
    };
    
    /**
     * Extending IncomingMessage to add useful methods
     */
    class RnbIncomingMessage extends IncomingMessage {

        /**
         * @returns {Promise<Buffer>} Buffer (can be empty)
         */
        async arrayBuffer() {
            const buffers = [];
            for await (const chunk of this) {
                buffers.push(chunk);
            }
            // eslint-disable-next-line no-undef
            return Buffer.concat(buffers);
        }
        /**
         * @returns {Promise<string>} request body as a string or null
         */
        async text() {
            const buffer = await this.arrayBuffer();
            if (buffer && buffer.length > 0) {
                return buffer.toString();
            }
            return null;
        }
            
        /**
         * @returns {Promise<object>} request body as json or null
         */
        async json() {
            const data = await this.text();
            if (data) {
                try {
                    return JSON.parse(data);
                } catch(err) {
                    console.warn(err);
                    console.log(data);
                }
            }
            return null;
        }
    }
}
