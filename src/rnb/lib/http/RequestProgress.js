

// https://developer.chrome.com/articles/fetch-streaming-requests/
const supportsRequestStreams = (() => {
    let duplexAccessed = false;
    const hasContentType = new Request('', {
        body: new ReadableStream(),
        method: 'POST',
        get duplex() {
            duplexAccessed = true;
            return 'half';
        },
    }).headers.has('Content-Type');
    return duplexAccessed && !hasContentType;
})();

let BANDWIDTH = Math.pow(10, 6);

const CHUNK_SIZE = 5000000;
const MEGA = Math.pow(10, 6);

/**
 * @typedef {CustomEvent<number>} RequestProgressEvent Detail is percentage of progress (0 - 100)
 */

/**
 * 
 * @param {number} value Progress 0 - 1
 * @returns {RequestProgressEvent}
 */
const createProgressEvent = value => new CustomEvent(RequestProgress.EVENT_PROGRESS, {detail: value * 100});

/**
 * @typedef UploadProgressOptions
 * @property {'bandwidth'|'reader'|'xhr'|'stream'} [mode='reader']
 * @property {string} [bandwidthUrl]
 */

/**
 * Make requests with the ability to monitor progress
 */
export default class RequestProgress extends EventTarget {

    /**
     * {@link RequestProgressEvent}
     */
    static EVENT_PROGRESS = 'progress';


    /**
     * @type {(url:string, file:Blob, start:number, total:number) => Promise}
     * @private
     */
    uploader = null;

    /**
     * @private
     */
    timer = null;

    /**
     * @type {RequestInit}
     */
    requestInit = {
        method: 'PUT',
        headers: {},
    };

    /**
     * @param {UploadProgressOptions} options
     * @param {RequestInit} requestInit
     */
    constructor(options, requestInit) {
        super();
        switch (options.mode) {
            case 'bandwidth':
                this.uploader = this.withBandwidth;
                break;
            case 'xhr':
                this.uploader = this.withXhr;
                break;
            case 'stream':
                if (!supportsRequestStreams) {
                    console.warn('Web browser does not support request stream ; switching to file reader');
                    this.uploader = this.withFileReader;
                } else {
                    this.uploader = this.withStream;
                }
                break;
            case 'reader':
            default:
                this.uploader = this.withFileReader;
                break;
        }
        this.requestInit = Object.assign({}, this.requestInit, requestInit);
    }

    /**
     * @param {RequestFilesMap} files
     * @param {(file: Blob, url: string) => void} [onFileLoadStart]
     * @param {RequestFileLoadEnd} [onFileLoadEnd]
     * @fires {@link EVENT_PROGRESS} multiple times until the upload is finished
     * @returns {Promise}
     */
    async request(files, onFileLoadStart, onFileLoadEnd) {
        let total = 0;
        let loaded = 0;
        Object.values(files).forEach(file => total += file.size);
        for (const [url, file] of Object.entries(files)) {
            if (onFileLoadStart) {
                onFileLoadStart(file, url);
            }
            await this.uploader(url, file, loaded, total);
            loaded += file.size;
            if (onFileLoadEnd && onFileLoadEnd(file, url)) {
                break;
            }
        }
    }

    /**
     * 
     * @param {string} url
     * @param {Blob} file 
     * @param {number} loaded Start in bytes
     * @param {number} total in bytes
     * @returns {Promise}
     * @private
     */
    async withStream(url, file, loaded, total) {
        const transformStream = new globalThis.TransformStream({
            start() {},
            transform: (chunk, controller) => {
                loaded += chunk.byteLength;
                this.dispatchEvent(createProgressEvent(loaded / total));
                controller.enqueue(chunk);
            },
            flush: () => {
                this.dispatchEvent(new Event('end'));
            },
        });
        this.requestInit.headers['content-type'] = 'application/octet-stream';
        this.requestInit.body = file.stream().pipeThrough(transformStream);
        this.requestInit.duplex = 'half';
        return globalThis.fetch(url, this.requestInit);
    }

    /**
     * @param {string} url
     * @param {Blob} file 
     * @param {number} loaded Start in bytes
     * @param {number} total in bytes
     * @private
     */
    async withFileReader(url, file, loaded, total) {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsArrayBuffer(file);
            fileReader.onload = async e => {
                const content = e.target.result;
                const nChunks = e.target.result.byteLength / CHUNK_SIZE;
                for (let i = 0; i < nChunks + 1; i += 1) {
                    loaded += CHUNK_SIZE;
                    this.dispatchEvent(createProgressEvent(loaded / total));
                    const body = content.slice(i * CHUNK_SIZE, (i + 1) * CHUNK_SIZE);
                    this.requestInit.headers['content-type'] = 'application/octet-stream';
                    this.requestInit.headers['content-length'] = body.length;
                    this.requestInit.body = body;
                    const res = await fetch(url, this.requestInit);
                    if (!res.ok) {
                        reject();
                        return;
                    }
                }
                resolve();
            };
        });
    }

    /**
     * @param {string} url
     * @param {Blob} file 
     * @param {number} loaded Start in bytes
     * @param {number} total in bytes
     * @returns {Promise}
     * @private
     */
    async withXhr(url, file, loaded, total) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onload = () => resolve();
            xhr.onerror = () => reject();
            xhr.upload.addEventListener('progress', e => {
                loaded += e.loaded;
                this.dispatchEvent(createProgressEvent(loaded / total));
            });
            xhr.open(this.requestInit.method, url, true);
            xhr.setRequestHeader('Content-Type', file.type);
            xhr.setRequestHeader('Content-Length', `${file.size}`);
            xhr.send(file);
        });
    }

    /**
     * @param {string} url
     * @param {Blob} file 
     * @param {number} loaded Start in bytes
     * @param {number} total in bytes
     * @returns {Promise}
     * @private
     */
    async withBandwidth(url, file, loaded, total) {
        globalThis.clearInterval(this.timer);
        this.dispatchEvent(createProgressEvent(loaded / total));
        this.timer = globalThis.setInterval(() => {
            loaded += BANDWIDTH;
            this.dispatchEvent(createProgressEvent(loaded / total));
        }, 1000);
        this.requestInit.headers['content-type'] = file.type;
        this.requestInit.headers['content-length'] = String(file.size);
        this.requestInit.body = file;
        const start = Date.now();
        const res = await fetch(url, this.requestInit);
        BANDWIDTH = Math.floor((file.size / ((Date.now() - start) / 1000)) / MEGA) * MEGA;
        console.log(BANDWIDTH / MEGA, 'Mo/s');
        globalThis.clearInterval(this.timer);
        this.timer = null;
        if (!res.ok) {
            return Promise.reject();
        }
        this.dispatchEvent(new Event('end'));
    }
}
