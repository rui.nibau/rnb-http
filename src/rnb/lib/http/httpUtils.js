/**@module rnb/lib/http/httpUtils */

/**
 * Regexp for leading slash
 *
 * @type {RegExp}
 */
export const RE_LEADING_SLASH = /^\//;
/**
 * Regexp for multiples slashs
 *
 * @type {RegExp}
 */
export const RE_MULTIPLE_SLASH = /([^:])\/{2,}/g;
/**
 * Regexp for trailing slash
 *
 * @type {RegExp}
 */
export const RE_TRAILING_SLASH = /\/$/;
/**
 * Regexp to anchor
 *
 * @type {RegExp}
 */
export const RE_ANCHOR = /#.*$/;
/**
 * Slash
 *
 * @type {string}
 */
export const SLASH = '/';
/**
 * @type {string}
 */
export const PROTOCOL_FILE = 'file:';

export const CHAR_CLASS_CHARS = '[0-9A-zÀ-ÿ.:_,\\|\\-% ()\']+';
export const CHAR_CLASS_PATH  = '[0-9A-zÀ-ÿ!.:_,\\|\\-/% ()\'\s]+';

/**
 * @param {string} url 
 * @returns {string}
 */
export const cleanMultipleSlash = url => url.replace(RE_MULTIPLE_SLASH, '$1/');

/**
 * @param {string} url 
 * @returns {string}
 */
export const cleanTrailingSlash = url => url.replace(RE_TRAILING_SLASH, '');

/**
 * @param {string} url 
 * @returns {string}
 */
export const cleanLeadingSlash = url => url.replace(RE_LEADING_SLASH, '');

/**
 * 
 * @param {string} path  Path to analsye
 * @returns {string} Cleaned path
 */
export const cleanPath = path => {
    path = cleanMultipleSlash(globalThis.decodeURI(path)).replace(RE_ANCHOR, '');
    if (path !== SLASH && path.endsWith(SLASH)) {
        path = path.slice(0, -1);
    }
    return path;
};

/**
 * @param {string} path Path given to a {@link Router}
 * @returns {RegExp} RegExp to be used by a {@link Router}
 */
export const buildRouteRegex = path => new RegExp(`^${path.split('/').map(part => {
    if (part.startsWith(':')) {
        return `(?<${part.substring(1)}>${CHAR_CLASS_CHARS})`;
    }
    return part;
}).join('/')}$`, 'i');

/**
 * '/foo/bar/baz' => ['foo', 'bar', 'baz']
 *
 * @param {string} path 
 * @returns {string[]}
 */
export const getSteps = path => path.split('/').filter(step => step !== '');

/**
 * Mime types
 */
export const MIME_TYPES = {
    cbz: 'application/zip',
    css: 'text/css',
    html: 'text/html',
    js: 'application/javascript',
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    json: 'application/json',
    pdf: 'application/pdf',
    png: 'image/png',
    xml: 'application/xml',
};

export const EXT_CBZ = '.cbz';
export const EXT_JPG = '.jpg';
export const EXT_PNG = '.png';

/**
 * @param {string} respath Path of a resource
 * @returns {string} Mime type based on the extension
 */
export const getMimetype = respath => {
    const dot = respath.lastIndexOf('.');
    const ext = dot > -1 ? respath.substring(dot+1) : null;
    return ext && MIME_TYPES[ext] ? MIME_TYPES[ext] : MIME_TYPES.html;
};

/**
 * @param {string} [basepath='/'] 
 * @returns {string}
 */
export const baseUrl = (basepath = '/') => {
    let url = `${globalThis.location.protocol}//${globalThis.location.host}`;
    if (basepath) {
        url += basepath;
    } else {
        url += globalThis.location.pathname.endsWith('.html')
            ? globalThis.location.pathname.substring(0, globalThis.location.pathname.lastIndexOf('/'))
            : globalThis.location.pathname;
    }
    if (url.endsWith('/')) {
        url = url.slice(0, -1);
    }

    return url;
};

/**
 * @param {string} path 
 * @returns {string}
 */
export const getPathname = path => {
    const base = path.startsWith('.') ? cleanPath(globalThis.location.href) : 'https://rnb.com';
    const url = new globalThis.URL(path, base);
    return cleanPath(url.pathname);
};
