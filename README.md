# rnb-http

Javascript library to manage http related stuff.

* [Website](https://omacronides.com/projets/rnb-http)
* [Source](https://framagit.org/rui.nibau/rnb-http)
* [Issues](https://framagit.org/rui.nibau/rnb-http/-/issues)
