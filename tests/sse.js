import Server from '../src/rnb/lib/http/Server.js';
import { sendResource } from '../src/rnb/lib/http/responses.js';

const interval = 5000;

const server = new Server();
server.sse('/sse');
server.get('/', async (req, res) => sendResource(res, './sse.html'));

globalThis.setInterval(function() {
    server.pushEvent('/sse', 'test', (new Date()).toLocaleTimeString());
}, interval);
