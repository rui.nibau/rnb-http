import { CHAR_CLASS_PATH } from '../src/rnb/lib/http/httpUtils.js';
import Router from '../src/rnb/lib/http/Router.js';

const fakeListener = () => Promise.resolve();

const testSimpleRoute = () => {
    console.group('Simple: /test-ok');
    
    const router = new Router();
    router.add('get', '/test-ok', fakeListener);
    let route;

    route = router.find('get', '/test-ok');
    console.assert(route !== null, 'Valid Route failed');

    route = router.find('get', '/test-ko');
    console.assert(route === null, 'Invalid Route passed');

    console.groupEnd();
};

const testGroups = () => {
    console.group('Groups: /api/:product/:id');
    
    const router = new Router();
    router.add('get', '/api/:product/:id', fakeListener);
    
    let route;

    // testing wrong apis
    route = router.find('get', '/api');
    console.assert(route === null, 'InValid Route passed');

    route = router.find('get', '/api/book');
    console.assert(route === null, 'InValid Route passed');
    
    // testing
    route = router.find('get', '/api/book/25');
    console.assert(route !== null, 'Valid Route failed');
    console.assert(route.vars !== null || route.vars !== undefined, 'Groups not there');
    if (!route.vars) {
        return;
    }
    console.assert(route.vars.product === 'book', `Group value "${route.vars.product}" is not "book"`);
    console.assert(route.vars.id === '25', `Group value "${route.vars.id}" is not 25`);

    console.groupEnd();
};

const testUserGroups = () => {
    console.group('UserGroups: /api/(?<respath>.+)');

    const router = new Router();
    router.add('get', `/api/(?<respath>${CHAR_CLASS_PATH})`, fakeListener);

    let route = router.find('get', '/api/path/to/resource');
    console.assert(route !== null, 'Valid route failed.');
    console.assert(route.vars !== null || route.vars !== undefined, 'Groups not there');
    if (!route.vars) {
        return;
    }
    console.assert(route.vars.respath === 'path/to/resource', `Wrong group value: ${route.vars.respath}`);

    console.groupEnd();
};

const testParams = () => {
    console.group('Params: /api?product=book&id=25');
    
    const router = new Router();
    router.add('get', '/api', fakeListener);
    
    let route;

    // testing wrong apis
    route = router.find('get', '/api/book/25');
    console.assert(route === null, 'InValid Route passed');

    route = router.find('get', '/api?product=book&id=25');
    console.assert(!route.vars, 'Invalid route group');
    console.assert(route.params.has('product'), 'No "product" param');

    console.groupEnd();
};

(async () => {
    console.group('ServerRouter');
    testSimpleRoute();
    testGroups();
    testUserGroups();
    testParams();
    console.groupEnd();
})();

export default {};