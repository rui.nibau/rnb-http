---
title:  rnb/lib/http/Client
date:   2022-10-08
intro:  Module qui permet de créer un objet de gestion de requêtes ``GET`` côté navigateur web.
---

## Présentation

La classe ``Client`` du module ``rnblib/http/Client`` permet de gérer des requêtes GET côté navigateur web. elle fait globalement deux choses :

• Instancier [un router](./router) pour dispatcher les requêtes GET vers des gestionnaires de routes (``RouteHandler``).
• Intercepter les clicks sur des liens absolus commençant par « / » où des liens relatifs commençant par « ./ » où « ../ » pour les intercepter et les rediriger vers le routeur tout en insérant une entrée dans l'historique.


°°stx-html°°
    <!DOCTYPE html>
    <html>
        <head>
            <title>rnb/lib/http/Client</title>
        </head>
        <body>
            <p><a href="/books">Go to books</a></p>
            <p><a href="/books/my-book">Go to book « my-book »</a></p>    
            <script src="main.js" type="module"></script>
        </body>
    </html>

°°stx-js°°
    import Client from 'rnb/lib/http/Client';
    
    const client = new Client();
    
    client.add('/books', () => {
        console.log('Display all books');
    });
    
    client.add('/book/:id', vars => {
        console.log(`Display book with id ${vars.id}`);
    });

