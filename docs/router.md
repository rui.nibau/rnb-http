---
title:  rnb/lib/http/Router
date:   2022-09-24
intro:  Le module ``rnb/lib/http/Router`` permet de créer un objet de gestion de routes http 
---

°°todo°°En cours de rédaction...

## Création de routes

°°stx-js°°
    import Router from '/path/to/rnb/lib/http/Router.js';
    
    const router = new Router();

### Frontend

°°stx-js°°
    router.add('get', '/books/:title', (vars, params) => {
        const title = vars.title;
    });

### Backend (node.js)

With the ``add()`` api :

°°stx-js°°
    router.add('get', '/books/:title', (req, res, vars, params) => {
        const title = vars.title;
    });
    router.add('get', '/books/:title', (req, res, vars, params) => {
        const title = vars.title;
        // read body from req
    });

With the ``path()`` api :

°°stx-js°°
    const router = new Router();
    router.path('/books/:title')
        // get book
        .get((req, res, vars) => {
            const title = vars.title;
            // Return book in res
        })
        // create book
        .put((req, res, vars) => {
            const title = vars.title;
            // read body from req
        })
        // update book
        .post((req, res, vars) => {
            const title = vars.title;
            // read body from req
        })
        // delete book
        .delete((req, res, vars) => {
            const title = vars.title;
            // delete ressource
        });


## Utilisation du router

°°stx-js°°
    const route = router.find('get', '/books/book_title');
    if (route !== null) {
        // Variables in the url pathname
        console.log(route.vars); // => {title: 'book_title'}
        // URLSearchParams
        console.log(route.params);
        // route handler, added with ``add``, ``path`` or ``global`` methods
        console.log(route.handler);
    }

Le router est utilisé par ``Client`` et par ``Server``.

## APIs

[°°api°°iframe:](/lab/rnb-http/jsdoc/module-rnb_http_Router.html)


