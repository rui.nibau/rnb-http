---
title:      rnb-http : Historique
date:       2022-04-13
updated:    2024-02-04
---

Voir aussi [l'historique rnb-js](/projets/rnb-js/#historique)

°°changelog°°
0.2.0 øø(2024-01-06)øø:
    • add: RequestProgress (WIP).
    • fix: request alternative for node.
    • upd: Client event dispatch
0.1.0 øø(2023-10-02)øø:
    • upd: versionning project on gitlab
2022-09-18:
    • add: Server
2022-04-13:
    • add: Initial commit after project splitting

