---
title:      rnb-http
date:       2022-09-25
updated:    2024-02-04
cats:       [web]
tags:       [rnb-js, code, http, requetes]
techs:      [javascript, nodejs, deno]
itemtype:   WebApplication
projects:   [rnb-http, rnb-js]
source:     https://framagit.org/rui.nibau/rnb-http
issues:     https://framagit.org/rui.nibau/rnb-http/-/issues
intro:      rnb-http est un projet web issue de [rnb-js](/projets/rnb-js)
status:     in-process
---

## Présentation

°°todo°°À écrire...

## Composition

[rnb/lib/http/Client](/projets/rnb-http/client):
    Application web côté navigateur.
rnb/lib/http/request:
    Un wrapper pour les requêtes http. La fonction à la même signature que l'API fetch. Différence : la promesse d'exécution est rejetée si la réponse n'est pas `ok`.
[rnb/lib/http/Router](/projets/rnb-http/router):
    Routage d'urls, côté navigateur ou côté serveur.
rnb/htpp/Server:
    Application côté serveur pour gérer une APIs d'urls.

[Documentation du code](/lab/rnb-http/jsdoc/index.html)


°°|hack°°
## Hack

°°lang-bash°°
    # Clone projet
    git clone https://framagit.org/rui.nibau/rnb-http.git

If you want to setup the project automatically (clonig dependencies and creating symlinks) :

°°lang-bash°°
    cd rnb-http
    ./project.sh setup

If you already have dependencies somewhere :

    cd rnb-http/deps
    # for each dependency
    ln -s </path/to/dependency> <dependency>
    cd ..
    # create symlinks
    ./project.sh symlinks


°°|changelog°°
## Historique

..include::./changelog.md


°°|licence°°
## Licence

..include::./licence.md

